package mth.mobile.ntnu.com.md_lab3;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.SensorEvent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import static android.app.PendingIntent.getActivity;

public class Ball extends View {
    private static final Paint FENCE_PAINT = new Paint();
    private static final Paint BALL_PAINT = new Paint();
    private static final int BALL_RADIUS = 80;
    private static final int MARGIN_EDGE = 40;
    private static final int STROKE_WIDTH = 30;
    private static final int VELOCITY = 3;
    private static final int BOUNCE_LENGTH = 20;
    private static Rect fence;
    private Vibrator vibrator;
    private WindowManager windowManager;
    private int windowWidth;
    private int windowHeight;

    //Boundaries
    private int xEdgeTop;
    private int yEdgeTop;
    private int edgeBottom;

    //Ball
    private int posX;
    private int posY;

    public Ball(Context context) {
        super(context);

        //Set up Vibrator in constructor because it needs the context
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        //Get window dimensions
        windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        assert windowManager != null;
        Display display = windowManager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int windowWidth = point.x;
        int windowHeight = point.y;

        //Fence
        fence = new Rect(MARGIN_EDGE, MARGIN_EDGE, windowWidth - MARGIN_EDGE, windowHeight - MARGIN_EDGE);
        FENCE_PAINT.setColor(Color.RED);
        FENCE_PAINT.setAlpha(100);
        FENCE_PAINT.setStrokeWidth(STROKE_WIDTH);
        xEdgeTop = windowWidth - MARGIN_EDGE - STROKE_WIDTH;
        yEdgeTop = windowHeight - MARGIN_EDGE - STROKE_WIDTH;
        edgeBottom = MARGIN_EDGE + STROKE_WIDTH;

        //Ball
        posX = windowWidth / 2;
        posY = windowHeight / 2;
        BALL_PAINT.setColor(Color.GREEN);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(fence, FENCE_PAINT);
        canvas.drawCircle(posX, posY, BALL_RADIUS, BALL_PAINT);
        invalidate();
    }

    /**
     * Function that proposes a new location for the ball, based on values from the sensor.
     * Also checks whether or not the proposed location is valid, taking the edges into consideration.
     * Finally updates the position values for the X and Y axis for the ball.
     *
     * @param event
     */
    public void updateBallPosition(SensorEvent event) {
        float newPosX = posX + (int) event.values[1];
        float newPosY = posY + (int) event.values[0];
        Log.d("X", String.valueOf(Math.abs(posX) - Math.abs(event.values[1])));
        Log.d("Y", String.valueOf(Math.abs(posY) - Math.abs(event.values[0])));

        // *************** Checks along the X-axis ***************

            // If proposed new position exceeds or is at the top edge
            if (newPosX + STROKE_WIDTH >= xEdgeTop) {
                vibrate();
                makeSound();

                for (int i = 0; i <= BOUNCE_LENGTH; i++) {
                    posX = posX - i;
                }
            }

            // If proposed new position exceeds or is at the lower edge
            else if (newPosX - STROKE_WIDTH <= edgeBottom) {
                vibrate();
                makeSound();

                for (int i = 0; i <= BOUNCE_LENGTH; i++) {
                    posX = posX + i;
                }
            }

            // If new proposed position is at any other location on the board
            else {
                posX = (int) (posX + VELOCITY * 0.5 * event.values[1]);
            }

        // *************** Checks along the Y-axis ***************

            // If proposed new position exceeds or is at the top edge
            if (newPosY + STROKE_WIDTH >= yEdgeTop) {
                vibrate();
                makeSound();

                for (int i = 0; i <= BOUNCE_LENGTH; i++) {
                    posY = posY - i;
                }
            }

            // If proposed new position exceeds or is at the lower edge
            else if (newPosY - STROKE_WIDTH <= edgeBottom) {
                vibrate();
                makeSound();

                for (int i = 0; i <= BOUNCE_LENGTH; i++) {
                    posY = posY + i;
                }
            }

            // If new proposed position is at any other location on the board
            else {
                posY = (int) (posY + VELOCITY * 0.5 * event.values[0]);
            }
        invalidate();
    }

    /**
     * Taken straight from https://stackoverflow.com/a/13950364
     */
    private void vibrate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            vibrator.vibrate(500);
        }
    }

    /**
     * taken straight from https://stackoverflow.com/a/29509305
     */
    private void makeSound() {
        ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
        toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 50);
    }
}
